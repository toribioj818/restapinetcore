using System;
using System.Net;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace API.Middleware
{
    public class HandlerErrorMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger<HandlerErrorMiddleware> logger;
        public HandlerErrorMiddleware(RequestDelegate _next, ILogger<HandlerErrorMiddleware> _logger)
        {
            next = _next;
            logger = _logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception e)
            {
                await HandlerExceptionAsync(context, e, logger);
            }
        }

        private async Task HandlerExceptionAsync(HttpContext context, Exception e, ILogger<HandlerErrorMiddleware> _logger)
        {
            object error = null;

            switch (e)
            {
                case HandlerException me :
                    _logger.LogError(e, "Manejador Error");
                    error = me.error;
                    context.Response.StatusCode = (int)me.code;
                    break;
                
                case Exception ex :
                    _logger.LogError(e, "Error de servidor");
                    error = string.IsNullOrWhiteSpace(ex.Message) ? "Error" : ex.Message;
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            context.Response.ContentType = "application/json";

            if (error != null)
            {
                var result = JsonConvert.SerializeObject(new { error });
                await context.Response.WriteAsync(result);
            }
        }
    }
}