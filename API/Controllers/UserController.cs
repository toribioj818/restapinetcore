using System.Threading.Tasks;
using Aplicacion.Security;
using Dominio;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [AllowAnonymous]
    public class UserController : BaseControl
    {
        [HttpPost("Login")]
        public async Task<ActionResult<UserData>> Login(Login.Execute data)
        {
            return await mediator.Send(data);
        }
    }
}