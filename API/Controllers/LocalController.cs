using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aplicacion.Locals;
using Dominio;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocalController : BaseControl
    {
        [HttpGet]
        public async Task<ActionResult<List<Local>>> GetLocal()
        {
            return await mediator.Send(new Query.LocalList());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Local>> LocalById(Guid id_local)
        {
            return await mediator.Send(new QueryById.LocalUnique { id_local = id_local });
        }

        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Execute data)
        {
            return await mediator.Send(data);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Unit>> Edit(Guid id_local, Edit.Execute data)
        {
            data.id_local = id_local;
            return await mediator.Send(data);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id_local)
        {
            return await mediator.Send(new Delete.Execute { id_local = id_local });
        }
    }
}