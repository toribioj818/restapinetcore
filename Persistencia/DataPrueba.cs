using System.Linq;
using System.Threading.Tasks;
using Dominio;
using Microsoft.AspNetCore.Identity;

namespace Persistencia
{
    public class DataPrueba
    {
        public static async Task InsertData(DatabaseContext context, UserManager<User> userManager)
        {
            if (!userManager.Users.Any())
            {
                var user = new User
                {
                    full_name = "Pepe Cadena",
                    UserName = "ElPepe07",
                    Email = "pepe07@gmail.com"
                };
                await userManager.CreateAsync(user, "Elpepe12345$");
            }
        }
    }
}