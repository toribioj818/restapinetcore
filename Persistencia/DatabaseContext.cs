using Dominio;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Persistencia
{
    public class DatabaseContext : IdentityDbContext<User>
    {
        public DatabaseContext(DbContextOptions options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Relatioship Client-ClientRoom
            modelBuilder.Entity<Client>()
                .HasOne(b => b.ClientRoom)
                .WithOne(i => i.Client)
                .HasForeignKey<ClientRoom>(b => b.id_client);
            
            // Relatioship User-Client
            modelBuilder.Entity<User>()
                .HasOne(b => b.Client)
                .WithOne(i => i.User)
                .HasForeignKey<Client>(b => b.Id);
            
            // Relatioship User-Employee
            modelBuilder.Entity<User>()
                .HasOne(b => b.Employee)
                .WithOne(i => i.User)
                .HasForeignKey<Employee>(b => b.Id);

            // Relatioship User-Local
            modelBuilder.Entity<User>()
                .HasOne(b => b.Local)
                .WithOne(i => i.User)
                .HasForeignKey<Local>(b => b.Id);

            // Relatioship User-Room
            modelBuilder.Entity<User>()
                .HasOne(b => b.Room)
                .WithOne(i => i.User)
                .HasForeignKey<Room>(b => b.Id);

            // Primary key
            modelBuilder.Entity<Local>().HasKey(b => b.id_local);
            modelBuilder.Entity<Client>().HasKey(b => b.id_client);
            modelBuilder.Entity<Employee>().HasKey(b => b.id_employee);
            modelBuilder.Entity<Room>().HasKey(b => b.id_room);
            modelBuilder.Entity<ClientRoom>().HasKey(b => b.id_client_room);    
        }

        public DbSet<User> User { get; set; }
        public DbSet<Local> Local { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Room> Room { get; set; }
        public DbSet<ClientRoom> ClientRoom { get; set; }
    }
}