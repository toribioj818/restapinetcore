using System;

namespace Dominio
{
    public class ClientRoom
    {
        public Guid id_client_room { get; set; }
        public Guid id_room { get; set; }
        public Guid id_client  { get; set; }
        public string full_name { get; set; }
        public DateTime? admission_date { get; set; }
        public Room Room { get; set; }
        public Client Client { get; set; }
    }
}