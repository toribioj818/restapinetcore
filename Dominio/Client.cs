using System;

namespace Dominio
{
    public class Client
    {
        public Guid id_client { get; set; }
        public Guid id_local { get; set; }
        public string Id { get; set; }
        public string user_name { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public User User { get; set; }
        public Local Local { get; set; }
        public ClientRoom ClientRoom { get; set; }
    }
}