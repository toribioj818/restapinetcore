using Microsoft.AspNetCore.Identity;

namespace Dominio
{
    public class User : IdentityUser
    {
        public string full_name { get; set; }
        public Local Local { get; set; }
        public Client Client { get; set; }
        public Employee Employee { get; set; }
        public Room Room { get; set; }
    }
}