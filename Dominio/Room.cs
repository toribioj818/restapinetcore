using System;
using System.Collections.Generic;

namespace Dominio
{
    public class Room
    {
        public Guid id_room { get; set; }
        public Guid id_local { get; set; }
        public string Id { get; set; }
        public string owner_name { get; set; }
        public string description { get; set; }
        public DateTime? creation_date { get; set; }
        public User User { get; set; }
        public Local Local { get; set; }
        public ICollection<ClientRoom> ClientRooms { get; set; }
    }
}