using System;
using System.Collections.Generic;

namespace Dominio
{
    public class Local
    {
        public Guid id_local { get; set; }
        public string name_local { get; set; }
        public string owner_name { get; set; }
        public string location { get; set; }
        public string description { get; set; }
        public string cover_foto { get; set; }
        public string Id { get; set; }
        public User User { get; set; }
        public ICollection<Client> Clients { get; set; }
        public ICollection<Employee> Employees { get; set; }
        public ICollection<Room> Rooms { get; set; }
    }
}