using System;

namespace Aplicacion.Security
{
    public class UserData
    {
        public string Id { get; set; }
        public string full_name { get; set; }
        public string token { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string photo { get; set; }
        public string Local { get; set; }
        public string Client { get; set; }
        public string Employee { get; set; }
        public string Room { get; set; }
    }
}