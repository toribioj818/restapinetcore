using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.Contracts;
using Aplicacion.HandlerError;
using Dominio;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistencia;

namespace Aplicacion.Security
{
    public class Login
    {
        public class Execute : IRequest<UserData>
        {
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class Validate : AbstractValidator<Execute>
        {
            public Validate()
            {
                RuleFor(x => x.Email).NotEmpty();
                RuleFor(x => x.Password).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Execute, UserData>
        {
            private readonly UserManager<User> userManager;
            private readonly SignInManager<User> manager;
            private readonly IJWTGenerator jWTGenerator;
            private readonly DatabaseContext context;
            public Handler(UserManager<User> _userManager, SignInManager<User> _manager, IJWTGenerator _jWTGenerator, DatabaseContext _context)
            {
                manager = _manager;
                userManager = _userManager;
                jWTGenerator = _jWTGenerator;
                context = _context;
            }

            public async Task<UserData> Handle(Execute request, CancellationToken cancellationToken)
            {
                // Verifica que el email exista
                var user = await userManager.FindByEmailAsync(request.Email);
                if (user == null)
                {
                    throw new HandlerException(HttpStatusCode.Unauthorized);
                }

                var result = await manager.CheckPasswordSignInAsync(user, request.Password, false);

                if (result.Succeeded)
                {
                    // Busca, validar y obtener datos relacionados
                    var searchLocal = await context.Local.Where(b => b.Id == user.Id).SingleOrDefaultAsync();
                    var searchClient = await context.Client.Where(b => b.Id == user.Id).SingleOrDefaultAsync();
                    var searchEmployee = await context.Employee.Where(b => b.Id == user.Id).SingleOrDefaultAsync();
                    var searchRoom = await context.Room.Where(b => b.Id == user.Id).SingleOrDefaultAsync();

                    // Local
                    if (searchLocal != null)
                    {
                        var local = context.Local.Single(x => x.Id == user.Id);
                        var id = local.id_local.ToString();

                        // Busca si tiene una room creada
                        if (searchRoom != null)
                        {
                            var room = context.Room.Single(x => x.Id == user.Id);
                            var id_r = room.id_room.ToString();

                            return new UserData
                            {
                                Id = user.Id,
                                full_name = user.full_name,
                                Username = user.UserName,
                                Email = user.Email,
                                photo = null,
                                token = jWTGenerator.CreateToken(user),
                                Local = id,
                                Client = null,
                                Employee = null,
                                Room = id_r
                            };
                        }

                        return new UserData
                        {
                            Id = user.Id,
                            full_name = user.full_name,
                            Username = user.UserName,
                            Email = user.Email,
                            photo = null,
                            token = jWTGenerator.CreateToken(user),
                            Local = id,
                            Client = null,
                            Employee = null,
                            Room = null
                        };
                    }

                    // Client
                    if (searchClient != null)
                    {
                        var client = context.Client.Single(x => x.Id == user.Id);
                        var id = client.id_client.ToString();

                        return new UserData
                        {
                            Id = user.Id,
                            full_name = user.full_name,
                            Username = user.UserName,
                            Email = user.Email,
                            photo = null,
                            token = jWTGenerator.CreateToken(user),
                            Local = null,
                            Client = id,
                            Employee = null,
                            Room = null
                        };
                    }

                    // Employee
                    if (searchEmployee != null)
                    {
                        var employee = context.Employee.Single(x => x.Id == user.Id);
                        var id = employee.id_employee.ToString();

                        // Busca si tiene salas creadas
                        if (searchRoom != null)
                        {
                            var room = context.Room.Single(x => x.Id == user.Id);
                            var id_r = room.id_room.ToString();

                            return new UserData
                            {
                                Id = user.Id,
                                full_name = user.full_name,
                                Username = user.UserName,
                                Email = user.Email,
                                photo = null,
                                token = jWTGenerator.CreateToken(user),
                                Local = null,
                                Client = null,
                                Employee = id,
                                Room = id_r
                            };
                        }

                        return new UserData
                        {
                            Id = user.Id,
                            full_name = user.full_name,
                            Username = user.UserName,
                            Email = user.Email,
                            photo = null,
                            token = jWTGenerator.CreateToken(user),
                            Local = null,
                            Client = null,
                            Employee = id,
                            Room = null
                        };
                    }
                }

                throw new HandlerException(HttpStatusCode.Unauthorized);
            }
        }
    }
}