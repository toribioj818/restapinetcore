using Dominio;

namespace Aplicacion.Contracts
{
    public interface IJWTGenerator
    {
        string CreateToken(User user);
    }
}