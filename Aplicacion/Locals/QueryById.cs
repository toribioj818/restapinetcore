using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using Dominio;
using MediatR;
using Persistencia;

namespace Aplicacion.Locals
{
    public class QueryById
    {
        public class LocalUnique : IRequest<Local>
        {
            public Guid id_local { get; set; }
        }

        public class Handler : IRequestHandler<LocalUnique, Local>
        {
            private readonly DatabaseContext context;
            public Handler(DatabaseContext _context)
            {
                context = _context;
            }

            public async Task<Local> Handle(LocalUnique request, CancellationToken cancellationToken)
            {
                var local = await context.Local.FindAsync(request.id_local);

                if (local == null)
                {
                    throw new HandlerException(HttpStatusCode.NotFound, new { response = "Lo siento, hubo error en la busqueda." });
                }

                return local;
            }
        }
    }
}