using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using MediatR;
using Persistencia;

namespace Aplicacion.Locals
{
    public class Delete
    {
        public class Execute : IRequest
        {
            public Guid id_local { get; set; }
        }

        public class Handler : IRequestHandler<Execute>
        {
            private readonly DatabaseContext context;
            public Handler(DatabaseContext _context)
            {
                context = _context;
            }

            public async Task<Unit> Handle(Execute request, CancellationToken cancellationToken)
            {
                var local = await context.Local.FindAsync(request.id_local);

                if (local == null)
                {
                    throw new HandlerException(HttpStatusCode.NotFound, new { response = "Lo siento, hubo error en la busqueda." });
                }

                context.Remove(local);
                var result = await context.SaveChangesAsync();

                if (result > 0)
                {
                    return Unit.Value;
                }

                throw new Exception("Lo siento, no se pudieron guardar los cambios.");
            }
        }
    }
}