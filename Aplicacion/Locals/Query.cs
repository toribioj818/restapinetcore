using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dominio;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistencia;

namespace Aplicacion.Locals
{
    public class Query
    {
        public class LocalList : IRequest<List<Local>> { }

        public class Handler : IRequestHandler<LocalList, List<Local>>
        {
            private readonly DatabaseContext context;
            public Handler(DatabaseContext _context) {
                context = _context;
            }
            public async Task<List<Local>> Handle(LocalList request, CancellationToken cancellationToken)
            {
                var local = await context.Local.ToListAsync();
                
                return local;
            }
        }
    }
}