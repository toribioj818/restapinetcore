using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Aplicacion.HandlerError;
using FluentValidation;
using MediatR;
using Persistencia;

namespace Aplicacion.Locals
{
    public class Edit
    {
        public class Execute : IRequest
        {
            public Guid id_local { get; set; }
            public string name_local { get; set; }
            public string owner_name { get; set; }
            public string location { get; set; }
            public string description { get; set; }
            public string cover_foto { get; set; }
        }

        public class ExecuteValidation : AbstractValidator<Execute>
        {
            public ExecuteValidation()
            {
                RuleFor(x => x.name_local).NotEmpty();
                RuleFor(x => x.owner_name).NotEmpty();
                RuleFor(x => x.location).NotEmpty();
                RuleFor(x => x.description).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Execute>
        {
            private readonly DatabaseContext context;
            public Handler(DatabaseContext _context)
            {
                context = _context;
            }
            public async Task<Unit> Handle(Execute request, CancellationToken cancellationToken)
            {
                var local = await context.Local.FindAsync(request.id_local);

                if (local == null)
                {
                    throw new HandlerException(HttpStatusCode.NotFound, new { response = "Lo siento, hubo error en la busqueda." });
                }

                local.name_local = request.name_local ?? local.name_local;
                local.owner_name = request.owner_name ?? local.owner_name;
                local.location = request.location ?? local.location;
                local.description = request.description ?? local.description;
                local.cover_foto = request.cover_foto ?? local.cover_foto;

                var result = await context.SaveChangesAsync();

                if (result > 0)
                    return Unit.Value;

                throw new Exception("Lo siento, hubo un error al procesar los datos.");
            }
        }
    }
}