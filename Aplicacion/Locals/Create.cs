using System;
using System.Threading;
using System.Threading.Tasks;
using Dominio;
using FluentValidation;
using MediatR;
using Persistencia;

namespace Aplicacion.Locals
{
    public class Create
    {
        public class Execute : IRequest
        {
            public string name_local { get; set; }
            public string owner_name { get; set; }
            public string location { get; set; }
            public string description { get; set; }
            public string cover_foto { get; set; }
            public string Id { get; set; }
        }

        public class ExecuteValidation : AbstractValidator<Execute>
        {
            public ExecuteValidation()
            {
                RuleFor(x => x.name_local).NotEmpty();
                RuleFor(x => x.owner_name).NotEmpty();
                RuleFor(x => x.location).NotEmpty();
                RuleFor(x => x.description).NotEmpty();
                RuleFor(x => x.Id).NotEmpty();
            }
        }

        public class Handler : IRequestHandler<Execute>
        {
            private readonly DatabaseContext context;
            public Handler(DatabaseContext _context)
            {
                context = _context;
            }

            public async Task<Unit> Handle(Execute request, CancellationToken cancellationToken)
            {
                var local = new Local
                {
                    name_local = request.name_local,
                    owner_name = request.owner_name,
                    location = request.location,
                    description = request.description,
                    cover_foto = request.cover_foto,
                    Id = request.Id
                };

                context.Local.Add(local);
                var result = await context.SaveChangesAsync();

                if (result > 0)
                {
                    return Unit.Value;
                }
                else
                {
                    throw new Exception("Lo siento, pero ha ocurrido un error al procesar los datos.");
                }
            }
        }
    }
}