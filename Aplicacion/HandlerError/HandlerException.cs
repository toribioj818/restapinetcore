using System;
using System.Net;

namespace Aplicacion.HandlerError
{
    public class HandlerException : Exception
    {
        public HttpStatusCode code { get; }
        public object error { get; }
        public HandlerException(HttpStatusCode _code, object _error = null)
        {
            code = _code;
            error = _error;
        }
    }
}