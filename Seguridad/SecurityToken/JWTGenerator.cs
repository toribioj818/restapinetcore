using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Aplicacion.Contracts;
using Dominio;
using Microsoft.IdentityModel.Tokens;

namespace Seguridad.SecurityToken
{
    public class JWTGenerator : IJWTGenerator
    {
        public string CreateToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.UserName)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Lo unico seguro es la inseguridad"));
            var credenciales = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = credenciales
            };

            var handlerToken = new JwtSecurityTokenHandler();
            var token = handlerToken.CreateToken(tokenDescription);

            return handlerToken.WriteToken(token);
        }
    }
}